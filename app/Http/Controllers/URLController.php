<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUrlRequest;
use App\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class URLController extends Controller
{

    public function __construct(){}

    public function store(StoreUrlRequest $request) {
        $store = Auth::user()->urls()->create([
            'token' => $this->generate(),
            'url' => $request->url
        ]);

        if ($store) {
            Session::flash('notification', ['type' => 'success','message' => 'Succesvol een verkorte link aangemaakt!']);
        }

        return redirect()->back();
    }

    /**
     * Let the User delete the URL
     * @param Url $url
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Url $url) {

        if (Auth::user()->id !== $url->user_id)
            return redirect()->route('home')->with('notification', ['type' => 'danger','message' => 'Geen permissie om deze URL te verwijderen!']);

        $action = $url->delete();

        if (!$action)
            return redirect()->route('home')->with('notification', ['type' => 'danger','message' => 'Er ging iets mis met het verwijderen van de URL!']);

        return redirect()->route('home')->with('notification', ['type' => 'success','message' => 'Succesvol een URL verwijderd!']);
    }

    /**
     * Let the User redirect if user visits the short URL.
     * @param string $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirect(string $token) {
        $url = Url::where('token', $token)->first();

        if (!$url) {
            return redirect()->route('home')->with('notification', ['type' => 'warning','message' => 'Deze URL bestaat niet!']);
        }

        $url->addVisit()->save();

        return redirect()->away($url->url);
    }


    /**
     * Generating and random token for the short URL.
     * @param int $length
     * @return string
     */

    private function generate(int $length = 10) : string {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $token = '';

        for ($i = 0; $i < $length; $i++) {
            $token .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $token;
    }
}
