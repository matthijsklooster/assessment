<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $guarded = ['id'];

    public function addVisit() {
        $this->visits += 1;
        return $this;
    }
}
