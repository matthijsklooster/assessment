@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Jouw verkorte links') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('urls.store') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="url" placeholder="Jouw endpoint" class="form-control {{ $errors->has('url') ? 'is-invalid' : '' }}">

                            @if($errors->has('url'))
                                <span class="invalid-feedback">{{ $errors->first('url') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary float-right mb-2">Maak aan</button>
                        </div>
                    </form>

                    <table class="table">
                        <thead>
                            <th>URL</th>
                            <th>Endpoint</th>
                            <th>Visits</th>
                            <th></th>
                        </thead>

                        <tbody>
                            @foreach(Auth::user()->urls as $url)
                                <tr>
                                    <td>
                                        {{ $url->token }}
                                    </td>

                                    <td>
                                        {{ $url->url }}
                                    </td>

                                    <td>
                                        {{ $url->visits }}
                                    </td>

                                    <td>
                                        <form method="POST" action="{{ route('urls.delete', ['url' => $url]) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm">Verwijder</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
