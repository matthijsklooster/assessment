<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('{token}', ['uses' => 'URLController@redirect', 'as' => 'redirect']);

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'urls', 'as' => 'urls.'], function () {
        Route::post('store', ['uses' => 'URLController@store', 'as' => 'store']);
        Route::delete('{url}/delete', ['uses' => 'URLController@delete', 'as' => 'delete']);
    });
});
