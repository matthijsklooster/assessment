<?php

use Illuminate\Database\Seeder;

class UrlTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('urls')->insert([
            'user_id' => 1,
            'token' => 'DwXDSOxoSN',
            'url' => 'https://google.com',
        ]);
    }
}
